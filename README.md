API request examples

base API url
https://imdb-movie-2019.herokuapp.com/api/movies

filter by name (full text search)
https://imdb-movie-2019.herokuapp.com/api/movies?name=Star

filter by movie name and director name
https://imdb-movie-2019.herokuapp.com/api/movies?name=Star&director=George

filter by genre name
https://imdb-movie-2019.herokuapp.com/api/movies?genre=Drama
